//
//  JYCRequestModel.m
//  OCStudy
//
//  Created by jyc on 2017/7/18.
//  Copyright © 2017年 BlockCopy. All rights reserved.
//

#import "JYCRequestModel.h"

#import "JYCShowModel.h"
#import "AFNetworking.h"

#import "YYModel.h"

@implementation JYCRequestModel

-(void)requestDataSucess:(void (^)(NSMutableArray *dataArray))sucess failure:(void(^)(NSString *errorString))failure
{
    NSString *URL           = @"https://api.weibo.com/2/statuses/public_timeline.json";
    NSDictionary *parameter = @{ @"access_token": @"2.00NofgBD0L1k4pc584f79cc48SKGdD",
                                 @"count": @"100"
                                 };
    
    AFHTTPSessionManager *sessionManager = [AFHTTPSessionManager manager];
    
    [sessionManager GET:URL parameters:parameter progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self wl_dictionaryToModel:responseObject success:sucess];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        failure(error.domain);
        
    }];
}

-(void)wl_dictionaryToModel:(NSDictionary *)dictionary
                    success:(void (^)(NSMutableArray *dataArray)) succeess
{
    NSArray *statuses         = dictionary[@"statuses"];
    NSMutableArray <JYCShowModel *>*dataArray = [[NSMutableArray alloc] initWithCapacity:statuses.count];
    for (int i = 0; i < statuses.count; i++) {
        JYCShowModel *showModel = [[JYCShowModel alloc] init];
        showModel.text     = statuses[i][@"text"];
        showModel.userName = statuses[i][@"user"][@"screen_name"];
        showModel.iconURL  = [NSURL URLWithString:statuses[i][@"user"][@"profile_image_url"]];
        [dataArray addObject:showModel];
    }
    succeess(dataArray);

}

@end
