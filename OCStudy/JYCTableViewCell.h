//
//  JYCTableViewCell.h
//  OCStudy
//
//  Created by jyc on 2017/7/18.
//  Copyright © 2017年 BlockCopy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYCTableViewCell : UITableViewCell

@property (nonatomic, strong) NSURL    *iconURL;

@property (nonatomic, strong) NSString *nameString;

@property (nonatomic, strong) NSString *textString;

@end
