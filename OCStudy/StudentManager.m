//
//  StudentManager.m
//  OCStudy
//
//  Created by jyc on 2017/8/11.
//  Copyright © 2017年 BlockCopy. All rights reserved.
//

#import "StudentManager.h"
#import "FMDB.h"

@implementation StudentManager
{
    FMDatabase *_dataBase;
}

+(instancetype)shareManager
{
    static StudentManager *manager =nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        if (manager==nil) {
            
            manager =[[StudentManager alloc]init];
        }
        
    });
    
    return manager;
}

-(instancetype)init
{
    if (self =[super init]) {
        
        NSString *documentPath =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
        NSString *dbPath =[documentPath stringByAppendingPathComponent:@"DBDemo.db"];
        
        _dataBase =[[FMDatabase alloc]initWithPath:dbPath];
        
        if ([_dataBase open]) {
            
            NSString *sql = @"create table if not exists student(id varchar(128), name text,age integer,headImage blob)";
            
            if ([_dataBase executeUpdate:sql]) {
                NSLog(@"成功创建表格 student");
            }
        
        }
        [_dataBase close];
    
    }

    return self;
}

@end
