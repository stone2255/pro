//
//  JYCShowModel.h
//  OCStudy
//
//  Created by jyc on 2017/7/18.
//  Copyright © 2017年 BlockCopy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JYCShowModel : NSObject

@property (nonatomic, strong) NSURL *iconURL;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *text;

@end
