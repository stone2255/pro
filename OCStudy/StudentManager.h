//
//  StudentManager.h
//  OCStudy
//
//  Created by jyc on 2017/8/11.
//  Copyright © 2017年 BlockCopy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StudentManager : NSObject

+(instancetype)shareManager;
@end
