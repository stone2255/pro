//
//  JYCRequestModel.h
//  OCStudy
//
//  Created by jyc on 2017/7/18.
//  Copyright © 2017年 BlockCopy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JYCRequestModel : NSObject

-(void)requestDataSucess:(void (^)(NSMutableArray *dataArray))sucess failure:(void(^)(NSString *errorString))failure;

@end
